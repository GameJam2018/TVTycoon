#!/usr/bin/env python3

import pygame
from .states.main_play_state import MainPlayState
import os

screen_size = (1055, 720)

assets = {
    "background": {"file": "tvtycoon/assets/background.png", "inst": None},
    "startup_screen": {"file": "tvtycoon/assets/startup_screen.png", "inst": None},
    "city_full_newyork": {"file": "tvtycoon/assets/city_newyork.png", "inst": None},
    "city_surface_newyork": {"file": "tvtycoon/assets/city_newyork_small.png", "inst": None},
    "city_full_punk": {"file": "tvtycoon/assets/city_punk.png", "inst": None},
    "city_surface_punk": {"file": "tvtycoon/assets/city_punk_small.png", "inst": None},
    "mast_full": {"file": "tvtycoon/assets/mast_full.png", "inst": None},
    "mast_scaled": {"file": "tvtycoon/assets/mast_scaled.png", "inst": None},
    "mask_background": {"inst": None},
    "font": {"file": "tvtycoon/assets/OpenSansCondensed-Light.ttf", "inst": None},
    "border": {"file": "tvtycoon/assets/tv_border.png", "inst": None},
    "mask_buttons": {"inst": None},

}

consts = {
    "mast_build_cost": 1000,
    "mast_radius": 200,
    "mast_turn_cost": 200,
    "turn_multiplyer": 1,
    "end_game_cash": 10000,
    "start_game_cash": 3000
}

assets["background"]["inst"] = pygame.image.load(assets["background"]["file"])
assets["startup_screen"]["inst"] = pygame.image.load(assets["startup_screen"]["file"])

assets["city_full_newyork"]["inst"] = pygame.image.load(assets["city_full_newyork"]["file"])
assets["city_surface_newyork"]["inst"] = pygame.Surface((100, 120), pygame.SRCALPHA)
assets["city_surface_newyork"]["inst"].fill((20, 20, 20, 0))

assets["city_full_punk"]["inst"] = pygame.image.load(assets["city_full_punk"]["file"])
assets["city_surface_punk"]["inst"] = pygame.Surface((150, 120), pygame.SRCALPHA)
assets["city_surface_punk"]["inst"].fill((20, 20, 20, 0))

assets["mast_full"]["inst"] = pygame.image.load(assets["mast_full"]["file"])
assets["mast_scaled"]["inst"] = pygame.image.load(assets["mast_scaled"]["file"])

assets["mask_background"]["inst"] = pygame.Surface(screen_size, pygame.SRCALPHA)   # per-pixel alpha
assets["mask_background"]["inst"].fill((0,0,0,220)) # alpha level

assets["mask_buttons"]["inst"] = pygame.Surface((140, 150), pygame.SRCALPHA)   # per-pixel alpha
assets["mask_buttons"]["inst"].fill((50,50,50)) # alpha level

assets["border"]["inst"] = pygame.image.load(assets["border"]["file"])
assets["border"]["inst"] = pygame.transform.scale(assets["border"]["inst"],  screen_size)

assets["font"]["inst"] = pygame.font.Font(assets["font"]["file"], 30)

if __name__ == "__main__":
    pygame.init()
    pygame.mixer.init()
    pygame.mixer.music.load("tvtycoon/assets/music.mp3") 
    pygame.mixer.music.play(True, True)
    display_surface = pygame.display.set_mode(screen_size, pygame.HWSURFACE | pygame.DOUBLEBUF)
    state = MainPlayState(assets, consts)

    running = True
    clock = pygame.time.Clock()

    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                break
            state.handle_event(event)

        display_surface.fill((0, 0, 0))
        state.draw_event(display_surface)
        pygame.display.update()

        clock.tick(60)
