import pygame

class City :

    def __init__(self, id_, position, population, demand, image, scaled_image, name):
        """
        Create a new city.

        Args:
            id_ (int)
            position (pygame.Vector2)
            population (integer)
            demand (enum program)
        """
        self._id = id_
        self._position = position
        self._population = population
        self._demand = demand
        self._old_demand = self._demand
        self._masts = []
        self._full_image = image
        self._image = scaled_image
        self._name = name

    def get_id(self):
        """
        Returns:
            id_ (int)
        """
        return self._id

    def get_old_demmand(self):
        return self._old_demand

    def get_name(self):
        """
        Returns the name of the city

        Returns:
            (string): city name
        """
        return self._name

    def get_position(self):
        """
        Returns:
            position (pygame.Vector2)
        """
        return self._position
    
    def get_population(self):
        """
        Returns:
            population (integer)
        """
        return self._population

    def set_population(self, population):
        self._population = population

    def get_demand(self):
        """
        Returns:
            demand (enum program)
        """
        return self._demand

    def get_masts(self):
        """
        Returns:
            masts (List<Mast>)
        """
        return self._masts
    
    def add_mast(self, mast):
        """
        Adds a mast to a city.

        Args:   
            mast (Mast)
        """
        self._masts.append(mast)

    def remove_mast(self, mast):
        """
        Removes a mast from the city.

        Args:
            mast (Mast)
        """
        self._masts.remove(mast)
    
    def set_demand(self, demand):
        """
        Set an audience demand for a certain type of program.

        Args:
            demand (enum Program)
        """
        self._old_demand = self._demand
        self._demand = demand

    def calculate_revenue(self, reduction_factor):
        """
        Returns money generated by the city (per turn). Uses a reducing factor to keep income sensible.

        Args: 
            reduction_factor

        Returns:
            revenue (float)
        """
        return self.get_mood() * self._population * reduction_factor
    
    def get_mood(self):
        """
        Calculates mood based on demand.
        Returns a value between 0 and 1 indicating a happiness percentage.
        For now, binary happiness 0 :( 1 :)

        Returns: 
            mood (float)
        """

        for mast in self._masts:
            if mast.get_programming() == self._old_demand:
                return 1

        return 0

    def render(self, display):
        """
        Draws a city on the GUI
        """
        display.blit(self._image, self._position)
