import pygame

class Mast:

    def __init__(self, id_, owner_id, position, radius, cost_per_turn, image, image_full, cities):
        """
        Create a new mast

        Args:
            id (int): The unique id of the mast
            position (pygame.Vector2): The position of the mast
            radius (float): The radius of the masts broadcasting range
            cost_per_turn (float): The cost per turn of the mast
            image (pygame.image): A pygame image or drawable object to display
        """
        
        self._id = id_
        self._owner_id = owner_id
        self._position = position
        self._radius = radius
        self._cost_per_turn = cost_per_turn
        self._program = None
        self._image = image
        self._full_image = image_full
        self.update_cities_in_range(cities)

    def get_id(self):
        """
        Returns the id of the mast.

        Returns:
            (int): masts id
        """

        return self._id

    def update_cities_in_range(self, cities):
        """
        Updates all cities in its radius with its mast id

        Args:
            cities (List[City])
        """
        cities = self.get_cities_in_range(cities)

        for city in cities:
            city.add_mast(self)

    def get_cities_in_range(self, cities):
        """
        Returns a list of all cities in the masts radius

        Args:
            cities (List[City])
            radius (float): a radius to test within
        """

        return list(filter(lambda city: (city.get_position().distance_to(self.get_position()) < self._radius), cities))

    def set_programming(self, program):
        """
        Sets the program of the mast

        Args:
            program (Enum Program)
        """
        self._program = program

    def get_programming(self):
        """
        Returns the current program the mast is broadcasting

        Returns:
            (enum Program)
        """
        return self._program

    def change_radius(self, radius):
        """Sets a new mast radius.

        Args:
            radius (float)
            cities (List[City]): a list of all cities
        """
        self._radius = radius


    def get_radius(self):
        """
        Returns the current radius of the mast.

        Returns:
            (float)
        """
        return self._radius

    def get_position(self):
        """
        Returns the current position of the mast.

        Returns:
            (pygame.Vector2)
        """
        return self._position

    def render(self, screen):
        """
        Renders the mast onto the map.

        Args:
            screen (pygame.Screen)
        """
        screen.blit(self._image, self._position)

    def get_owner_id(self):
        """
        Returns the current owner id of the mast.

        Returns:
            (int): current owner id
        """
        return self._owner_id
