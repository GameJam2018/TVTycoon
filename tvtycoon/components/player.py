class Player:

    def __init__(self, id_, startingCash):
        """
        Create a new player.

        Args:
            _id (int): the id of the play
            startingCash (float): the starting cash of the player
        """
        self._id = id_
        self.cash = startingCash
        self.turns = 0

    def get_id(self):
        """
        Returns the players id

        Returns:
            id (int): the players id
        """
        return self._id

    def inc_turn(self):
        self.turns += 1

    def get_turns(self):
        return self.turns

    def get_masts(self, masts):
        """
        Filters the complete list of masts to return a list of masts owned by the player.

        Args:
            masts (List[Mast]): A list of all masts

        Returns:
            (List[Mast]): A filtered list of only the players masts
        """
        pass
    
    def set_cash(self, cash):
        """
        Set the players current cash.

        Args:
            cash (float): the new cash value
        """
        self.cash = cash

    def get_cash(self):
        """
        Retrieves the players current cash.

        Returns
            (float): the players current cash.
        """
        return self.cash

    