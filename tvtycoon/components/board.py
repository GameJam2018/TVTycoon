#!/usr/bin/env python3

import itertools
from itertools import cycle, dropwhile
from typing import Iterator, List
from .city import City
from .mast import Mast
from .player import Player
import pygame
from enum import Enum
import random

class Board:
    """This represents a board along with the rest of the game related state."""

    class Programming(Enum):
        ONE= 1
        TWO = 2
        THREE = 3

    def __init__(self):
        """Create a new empty board."""

        self._cities: List[City] = list()
        self._city_id_gen: Iterator(int) = itertools.count(0)

        self._masts: List[Mast] = list()
        self._mast_id_gen: Iterator(int) = itertools.count(0)

        self._players: List[Player] = list()
        self._player_id_gen: Iterator(int) = itertools.count(0)

        self.current_player_id = None

    def new_city(self, position, population, demand, image, image_scaled, name) -> City:
        """Create a new city.

        Args:
            position: The position to place the city at
            population: The initial population of the city
            demand: The initial demand of the city

        Returns:
            The city which was just created.
        """
        id_ = next(self._city_id_gen)
        new_city = City(id_, position, population, demand, image, image_scaled, name)
        self._cities.append(new_city)
        return new_city

    def get_city(self, id_: int) -> City:
        """Get a city with the given id

        Args:
            id_: The id of the city to retrieve

        Returns:
            The city requested.
        """
        return next((c for c in self._cities if c.get_id() == id_), None)

    def get_city_list(self) -> List[City]:
        """Get all of the cities on the board.

        Returns:
            A list of the cities
        """
        return self._cities

    def new_mast(self, position, radius, cost_per_turn, cost_to_build, mast_image, mast_image_scaled) -> Mast:
        """Create a new mast.

        Args:
            position: The position to place the mast at
            radius: The initial radius of the reach of the mast
            cost_per_turn: The initial cost per turn of the mast

        Returns:
            The mast which was just created
        """
        id_ = next(self._mast_id_gen)


        new_mast = Mast(
            id_, self.current_player_id, position, radius, cost_per_turn, mast_image, mast_image_scaled, self.get_city_list()
        )
        self._masts.append(new_mast)

        self.get_player(self.current_player_id).cash -= cost_to_build

        return new_mast

    def get_mast(self, id_: int) -> Mast:
        """Get a mast with the given id

        Args:
            id_: The id of the mast to retrieve

        Returns:
            The mast requested.
        """
        return next((m for m in self._masts if m.get_id() == id_), None)

    def get_mast_list(self) -> List[Mast]:
        """Get all of the masts on the board.

        Returns:
            A list of the masts
        """
        return self._masts

    def new_player(self, cash) -> Player:
        """Create a new player

        Args:
            cash: The initial amount of cash the player will have

        Returns:
            The player which was just created
        """
        id_ = next(self._player_id_gen)
        new_player = Player(id_, cash)
        self._players.append(new_player)

        # Assign a player to be the current player if no player is already set
        if self.current_player_id is None:
            self.current_player_id = id_

        return new_player

    def get_player(self, id_: int) -> Mast:
        """Get a player with the given id

        Args:
            id_: The id of the player to retrieve

        Returns:
            The player requested
        """
        return next((p for p in self._players if p.get_id() == id_), None)

    def get_player_list(self) -> List[Player]:
        """Get all of the players involved in the game.

        Returns:
            A list of the players
        """
        return self._players

    def change_cities_demmand(self):
        """
        Randomly calculate change in cities demand.

        _Very basic_
        """
        for city in self.get_city_list():

            if (random.randint(1, 10) < 3):
                city.set_demand(random.choice(list(self.Programming)))

    def change_cities_population(self):
        for city in self.get_city_list():
            variation = int(float(city.get_population()) / 10)
            new_population = city.get_population() + random.randint(-variation, variation)
            city.set_population(new_population)

    def advance_turn(self, turn_mutliplyer):
        if self.current_player_id is None:
            return

        revenue = 0
        expenditure = 0

        # Get all of the player's masts
        this_player_masts = [m for m in self._masts if m.get_owner_id() == self.current_player_id]

        # For each of the masts figure out what cities have been reached
        for city in self.get_city_list():
            revenue += city.calculate_revenue(turn_mutliplyer)

        for mast in this_player_masts:
            expenditure += mast._cost_per_turn

        player = self.get_player(self.current_player_id)
        player.inc_turn()
        player.cash += (revenue * turn_mutliplyer) - expenditure
        # TODO: Remove players when they run out of money

        def not_id_matches(other_player):
            other_player.get_id() != player.get_id()

        player_cycle = dropwhile(not_id_matches, cycle(self.get_player_list()))
        next(player_cycle)

        self.change_cities_demmand() #calculate new city demmand
        self.change_cities_population()

        self.current_player_id = next(player_cycle).get_id()
