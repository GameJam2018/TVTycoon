#!/usr/bin/env python3

from tvtycoon.components.board import Board
from tvtycoon.gui.map_gui import MapGUI
import pygame

class MainPlayState:
    def __init__(self, assets, consts):
        self.toplevel_gui = MapGUI(None, consts)

        self.board = Board()
        self.assets = assets
        self.consts = consts

        self.board.new_player(consts["start_game_cash"]) 
        
        self.board.new_city(pygame.math.Vector2(280, 100), 1000, self.board.Programming.ONE, self.assets["city_full_punk"]["inst"], self.assets["city_surface_punk"]["inst"], "Punkham")
        self.board.new_city(pygame.math.Vector2(200, 380), 700, self.board.Programming.TWO, self.assets["city_full_newyork"]["inst"], self.assets["city_surface_newyork"]["inst"], "Londonian")
        self.board.new_city(pygame.math.Vector2(600, 500), 200, self.board.Programming.TWO, self.assets["city_full_newyork"]["inst"], self.assets["city_surface_newyork"]["inst"], "Yark")

    def handle_event(self, event):
        self.toplevel_gui.handle_event(event, self.board, self.assets)

    def draw_event(self, display_surface):
        self.toplevel_gui.draw_event(display_surface, self.board, self.assets)
