import tvtycoon.gui.pygbutton as pygbutton
import tvtycoon.gui.add_mast_gui as add_mast_gui
import tvtycoon.gui.programme_gui as programme_gui
import tvtycoon.gui.city_gui as city_gui
import tvtycoon.gui.utils as utils
import tvtycoon.gui.render_components as render
import tvtycoon.gui.info_box_gui as info_box_gui
import tvtycoon.gui.popup_gui as popup_gui
import tvtycoon.gui.startup_gui as startup_gui

import pygame

CLICK_RADIUS = 40

class MapGUI:
    def __init__(self, parent, consts):
        self.parent_ref = parent
        self.MAP_POS = (20, 20)
        self.MAP_SIZE = (960, 650)
        self.child = startup_gui.StartupGUI(self, self.MAP_SIZE, self.MAP_POS)
        self.next_turn_button = pygbutton.PygButton((850, 510, 180, 50), 'Next Turn')
        self.add_mast_buton = pygbutton.PygButton((850, 570, 180, 50), 'Add Mast')
        self.info_box = info_box_gui.InfoBoxGUI(self, (850, 290), "Current Cash", 1000)
        self.consts = consts

    def close(self):
        if self.parent_ref is not None:
            self.parent_ref.child = None

    def _obj_clicked(self, mouse_pos, obj_list):
        for ob in obj_list:
            ob_position = ob.get_position()
            in_x = (mouse_pos[0]  > ob_position[0]) and (mouse_pos[0] < ob_position[0] + ob._image.get_width())
            in_y = (mouse_pos[1]  > ob_position[1]) and (mouse_pos[1] < ob_position[1] + ob._image.get_height())

            if (in_x and in_y):
                return ob

    def handle_event(self, event, board, assets):
        if self.child is not None:
            self.child.handle_event(event, board, assets)
        else:
            if 'click' in self.next_turn_button.handleEvent(event):
                board.advance_turn(self.consts["turn_multiplyer"])
                new_cash = board.get_player(board.current_player_id).get_cash() 
                if (new_cash < 0):
                    self.child = popup_gui.PopupGUI(self, self.MAP_SIZE, self.MAP_POS, "Game Over!", ["You ran out of cash.", "Total turns: {}".format(board.get_player(board.current_player_id).get_turns())], True)
                elif (new_cash >= self.consts["end_game_cash"]):
                    self.child = popup_gui.PopupGUI(self, self.MAP_SIZE, self.MAP_POS, "Congratulations!", ["You retire a rich, rich individual.", "Total turns: {}".format(board.get_player(board.current_player_id).get_turns())], True)
            elif 'click' in self.add_mast_buton.handleEvent(event):
                if (board.get_player(board.current_player_id).get_cash() >= self.consts["mast_build_cost"]):
                    self.child = add_mast_gui.AddMastGUI(self, self.consts["mast_radius"], self.consts["mast_turn_cost"], self.consts["mast_build_cost"], self.MAP_SIZE, self.MAP_POS)
                else:
                    self.child = popup_gui.PopupGUI(self, self.MAP_SIZE, self.MAP_POS, "Stop!", ["You can't afford this...", "Not enough cash :(", "You need: ${}".format(self.consts["mast_build_cost"])])
                    
            elif (event.type == pygame.MOUSEBUTTONUP) and (utils._mouse_in_range(pygame.mouse.get_pos(), self.MAP_POS, (self.MAP_POS[0] + self.MAP_SIZE[0], self.MAP_POS[1] + self.MAP_SIZE[1]))):
                mast_clicked = self._obj_clicked(pygame.mouse.get_pos(), board.get_mast_list())
                city_clicked = self._obj_clicked(pygame.mouse.get_pos(), board.get_city_list())

                if (mast_clicked != None):
                    self.child = programme_gui.ProgrammeGUI(self, mast_clicked, board, self.MAP_SIZE, self.MAP_POS)

                if (city_clicked != None):
                    self.child = city_gui.CityGUI(self, city_clicked, board, self.MAP_SIZE, self.MAP_POS)
                

    def draw_event(self, display_surface, board, assets):
        render.map(display_surface, board, assets, self.MAP_SIZE, self.MAP_POS)
        render.border(display_surface, board, assets)

        info_box_elements = ["${:,}".format(board.get_player(board.current_player_id).get_cash())]
        self.info_box.draw_event(display_surface, assets["font"]["inst"], info_box_elements)

        self.next_turn_button.draw(display_surface)
        self.add_mast_buton.draw(display_surface)

        if self.child is not None:
            self.child.draw_event(display_surface, board, assets)