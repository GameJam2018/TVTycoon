import tvtycoon.gui.pygbutton as pygbutton
import tvtycoon.gui.render_components as render
import tvtycoon.gui.info_box_gui as info_box_gui
import pygame

class StartupGUI:
    def __init__(self, parent, map_size, map_pos):
        self.parent_ref = parent
        self.child = None
        self.map_size = map_size
        self.map_pos = map_pos
        self.start_button = pygbutton.PygButton((850, 530, 180, 50), 'Start!')


    def close(self):
        if self.parent_ref is not None:
            self.parent_ref.child = None

    def handle_event(self, event, board, assets):
        if self.child is not None:
            self.child.handle_event(event)
        else:
            if 'click' in self.start_button.handleEvent(event):
                self.close()

    def draw_event(self, display_surface, board, assets):
        
        display_surface.blit(assets["startup_screen"]["inst"], self.map_pos)

        render.border(display_surface, board, assets)

        self.start_button.draw(display_surface)

        if self.child is not None:
            self.child.draw_event(display_surface)