import tvtycoon.gui.pygbutton as pygbutton
import tvtycoon.gui.render_components as render
import tvtycoon.gui.info_box_gui as info_box_gui
import pygame

class CityGUI:
    def __init__(self, parent, city, board, map_size, map_pos):
        self.parent_ref = parent
        self.child = None
        self.city = city
        self.map_size = map_size
        self.map_pos = map_pos
        self.info_box = info_box_gui.InfoBoxGUI(self, (850, 290), "Current Cash", 1000)

        self.close_button = pygbutton.PygButton((850, 570, 180, 50), 'Close')

    def close(self):
        if self.parent_ref is not None:
            self.parent_ref.child = None

    def handle_event(self, event, board, assets):
        if self.child is not None:
            self.child.handle_event(event)
        else:
            if 'click' in self.close_button.handleEvent(event):
                self.close()

    def draw_event(self, display_surface, board, assets):
        
        render.map(display_surface, board, assets, self.map_size, self.map_pos)
        #grey out the map with a translucent mask

        display_surface.blit(assets["mask_background"]["inst"], (0, 0))

        render.border(display_surface, board, assets)

        info_box_elements = ["${:,}".format(board.get_player(board.current_player_id).get_cash())]
        self.info_box.draw_event(display_surface, assets["font"]["inst"], info_box_elements)

        self.close_button.draw(display_surface)

        title = assets["font"]["inst"].render(self.city.get_name() + " City", True, (255, 255, 255))
        display_surface.blit(title, (350, 100))

        demmand = assets["font"]["inst"].render("Last surveyed demmand: {}".format(self.city.get_old_demmand()), True, (255, 255, 255))
        display_surface.blit(demmand, (50, 200))

        mood = assets["font"]["inst"].render("Last surveyed mood: {}".format(self.city.get_mood()), True, (255, 255, 255))
        display_surface.blit(mood, (50, 250))
        
        population = assets["font"]["inst"].render("Current population: {}".format(self.city.get_population()), True, (255, 255, 255))
        display_surface.blit(population, (50, 300))

        # TODO: Add list of channels being received by city

        # Display a big image of the city (show off the artwork!)
        display_surface.blit(self.city._full_image, (800 - self.city._full_image.get_width() , 650 - self.city._full_image.get_height(),))

        if self.child is not None:
            self.child.draw_event(display_surface)