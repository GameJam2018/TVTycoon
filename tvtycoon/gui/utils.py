def _mouse_in_range(mouse_pos, start_range, end_range):
    return ((mouse_pos[0] > start_range[0]) and (mouse_pos[0] < end_range[0]) and (mouse_pos[1] > start_range[1]) and (mouse_pos[1] < end_range[1]))
