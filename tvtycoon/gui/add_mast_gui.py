import tvtycoon.gui.pygbutton as pygbutton
import pygame
import tvtycoon.gui.programme_gui as programme_gui
import tvtycoon.gui.utils as utils
import tvtycoon.gui.render_components as render
import tvtycoon.gui.info_box_gui as info_box_gui

class AddMastGUI:
    def __init__(self, parent, radius, cost_per_turn, cost_to_build, map_size, map_pos):
        self.parent_ref = parent
        self.child = None
        self.radius = radius
        self.cost_per_turn = cost_per_turn
        self.cost_to_build = cost_to_build
        self.map_size = map_size
        self.map_pos = map_pos
        self.cancel_button = pygbutton.PygButton((850, 570, 180, 50), 'Cancel')
        self.info_box = info_box_gui.InfoBoxGUI(self, (850, 290), "Mast Cost", 1000)

    def close(self):
        if self.parent_ref is not None:
            self.parent_ref.child = None

    def handle_event(self, event, board, assets):
        if self.child is not None:
            self.child.handle_event(event, board, assets)
        elif 'click' in self.cancel_button.handleEvent(event):
            self.close()
        elif (event.type == pygame.MOUSEBUTTONUP) and (utils._mouse_in_range(pygame.mouse.get_pos(), self.map_pos, (self.map_pos[0] + self.map_size[0], self.map_pos[1] + self.map_size[1]))):
            image_size = pygame.math.Vector2(assets["mast_scaled"]["inst"].get_width(),  assets["mast_scaled"]["inst"].get_height())
            mouse_pos = pygame.math.Vector2(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])
            
            new_mast = board.new_mast(mouse_pos - (image_size / 2), self.radius, self.cost_per_turn, self.cost_to_build, assets["mast_scaled"]["inst"], assets["mast_full"]["inst"])

            self.parent_ref.child = programme_gui.ProgrammeGUI(self.parent_ref, new_mast, board, self.map_size, self.map_pos)
            
    def draw_event(self, display_surface, board, assets):
        render.map(display_surface, board, assets, self.map_size, self.map_pos)
        
        pos = pygame.mouse.get_pos()
        pygame.draw.circle(display_surface, (135, 133, 129, 128), pos, self.radius, 5)
        
        render.border(display_surface, board, assets)

        info_box_elements = ["P/ Turn: ${:,}".format(self.cost_per_turn), "To Build: ${:,}".format(self.cost_to_build)]
        self.info_box.draw_event(display_surface, assets["font"]["inst"], info_box_elements)

        self.cancel_button.draw(display_surface)

        #TODO: Add title with mast id
        #TODO: Add current channel selection text

        if self.child is not None:
            self.child.draw_event(display_surface, board, assets)


