import pygame

def map(display_surface, board, assets, map_size=(960, 550), map_pos=(90, 80)):
    sized_background = pygame.transform.scale(assets["background"]["inst"], map_size)
    display_surface.blit(sized_background, map_pos)

    for city in board.get_city_list():
        city.render(display_surface)

    for mast in board.get_mast_list():
        mast.render(display_surface)

def border(display_surface, board, assets):
    display_surface.blit(assets["border"]["inst"], (0, 0))