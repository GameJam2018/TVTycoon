import tvtycoon.gui.pygbutton as pygbutton
import tvtycoon.gui.render_components as render
import tvtycoon.gui.info_box_gui as info_box_gui
import pygame

class PopupGUI:
    def __init__(self, parent, map_size, map_pos, title, messages, end_game=False):
        self.parent_ref = parent
        self.child = None
        self.title = title
        self.map_size = map_size
        self.map_pos = map_pos
        self.messages = messages
        self.is_end_game = end_game

        self.close_button = pygbutton.PygButton((850, 570, 180, 50), 'Close')
        self.info_box = info_box_gui.InfoBoxGUI(self, (300, 300), "", 1000)


    def close(self):
        if self.parent_ref is not None:
            self.parent_ref.child = None

    def handle_event(self, event, board, assets):
        if self.child is not None:
            self.child.handle_event(event)
        else:
            if 'click' in self.close_button.handleEvent(event):
                if not self.is_end_game:
                    self.close()
                else:
                    pygame.quit()


    def draw_event(self, display_surface, board, assets):
        
        render.map(display_surface, board, assets, self.map_size, self.map_pos)
        #grey out the map with a translucent mask

        display_surface.blit(assets["mask_background"]["inst"], (0, 0))

        render.border(display_surface, board, assets)

        title = assets["font"]["inst"].render(self.title, True, (255, 255, 255))
        display_surface.blit(title, (350, 200))

        self.info_box.draw_event(display_surface, assets["font"]["inst"], self.messages)

        self.close_button.draw(display_surface)

        if self.child is not None:
            self.child.draw_event(display_surface)