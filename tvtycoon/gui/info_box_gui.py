import tvtycoon.gui.pygbutton as pygbutton
import tvtycoon.gui.render_components as render
import pygame

class InfoBoxGUI:
    def __init__(self, parent, pos, title, refresh_rate):
        """
        Infobox creates a alternating info box of values on the screen
        Args:
            parent (parent): The parent of the gui component
            pos (x, y): the position of the info box
            title (String): the title to be drawn
            refresh_rate (int): ms between refreshes of element
        """

        self.parent_ref = parent
        self.child = None
        self.position = pos
        self.title = title
        self.last_update_time = pygame.time.get_ticks()
        self.refresh_rate = refresh_rate
        self.element_index = 0

    def info_box(self,display_surface, text, font, position):
        render_inst = font.render(text, True, (255, 255, 255))
        display_surface.blit(render_inst, position)

    def close(self):
        if self.parent_ref is not None:
            self.parent_ref.child = None

    def handle_event(self, event, board, assets):
        if self.child is not None:
            self.child.handle_event(event)

    def draw_event(self, display_surface, font, elements):

        # Cycle through to the next element if necessary
        if (self.last_update_time + self.refresh_rate < pygame.time.get_ticks()):
            self.last_update_time = pygame.time.get_ticks()
            self.element_index = (self.element_index + 1) % len(elements)

        self.info_box(display_surface, self.title, font,  self.position)
        self.info_box(display_surface, elements[self.element_index], font,  (self.position[0], self.position[1] + 40))
        
        if self.child is not None:
            self.child.draw_event(display_surface)