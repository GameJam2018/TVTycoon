import tvtycoon.gui.pygbutton as pygbutton
import tvtycoon.gui.render_components as render
import tvtycoon.gui.info_box_gui as info_box_gui
import pygame

class ProgrammeGUI:
    def __init__(self, parent, mast, board, map_size, map_pos):
        self.parent_ref = parent
        self.child = None
        self.mast = mast
        self.map_size = map_size
        self.map_pos = map_pos

        self.info_box = info_box_gui.InfoBoxGUI(self, (850, 290), "Current Cash", 1000)
        self.current_broadcasting_box = info_box_gui.InfoBoxGUI(self, (520, 140), "", 1000)

        self.buttons = {}
        current_button_y = 250
        button_y_spacing = 70

        for programme in board.Programming:
            self.buttons[pygbutton.PygButton((90, current_button_y, 200, 50), str(programme))] = programme
            current_button_y =  current_button_y + button_y_spacing


    def close(self):
        if self.parent_ref is not None:
            self.parent_ref.child = None

    def handle_event(self, event, board, assets):
        if self.child is not None:
            self.child.handle_event(event)
        else:
            for button in self.buttons.keys():
                if 'click' in button.handleEvent(event):
                    self.mast.set_programming(self.buttons[button])
                    self.close()
                    break

    def draw_event(self, display_surface, board, assets):
        
        render.map(display_surface, board, assets, self.map_size, self.map_pos)
        #grey out the map with a translucent mask

        display_surface.blit(assets["mask_background"]["inst"], (0, 0))

        render.border(display_surface, board, assets)

        info_box_elements = ["${:,}".format(board.get_player(board.current_player_id).get_cash())]
        self.info_box.draw_event(display_surface, assets["font"]["inst"], info_box_elements)

        #TODO: Center this title
        title = assets["font"]["inst"].render("Mast: {}".format(self.mast.get_id()), True, (255, 255, 255))
        display_surface.blit(title, (360, 100))

        select_programming_heading = assets["font"]["inst"].render("Select Programming".format(self.mast.get_id()), True, (255, 255, 255))
        display_surface.blit(select_programming_heading, (90, 180))
        
        current_programming_elements = ["Broadcasting LIVE", str(self.mast.get_programming())]
        self.current_broadcasting_box.draw_event(display_surface, assets["font"]["inst"], current_programming_elements)

        display_surface.blit(self.mast._full_image, (500, 250))

        for button in self.buttons.keys():
            button.draw(display_surface)

        if self.child is not None:
            self.child.draw_event(display_surface)
