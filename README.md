# ColourBoxTV

* Sorry, we didn't have time to make an executable... So, to run this game, download the code, install python3 and pygame, and then do `$ python3 -m tvtycoon`. We'll try and upload one in the future though!

Let ColourBoxTV transport you back to the 1950's. You're a tycoon, out to make some cash and brainwash some citizens. You start off with a wad of cash, and mustto build TV transmitters across the country to satisfy cities insatible desire for the newfangled television.

# Credits:

* Aleksander Aare (instagram.com/a.aare) : Artist
* Anjali Trace : Developer and Graphics Designer
* Jacob Unwin : Developer
* Peter Ryan Smith : Developer
* Jack Romo : First night brainstorming

# License

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.